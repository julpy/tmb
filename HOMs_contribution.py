#testtest
#calculate contributon on monomer and dimers to total signal

import os
import matplotlib
matplotlib.use('Qt5Agg')   # generate postscript output by default

import numpy as np
from scipy import optimize
from pygaero import pio
from pygaero import therm
from pygaero import gen_chem



from scipy.special import erf
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
from pygaero import therm
import numpy as np
from scipy.signal import argrelmin
from scipy.signal import argrelmax

import matplotlib.ticker as plticker
import matplotlib.dates as md
import re



def save_results(indir, df_ls_input, name):
    df_to_save = df_ls_input

    # make a list with filenames, needed for TMax export
    file_names = [f for f in os.listdir(indir) if f.endswith('.csv')]
    filenames = []
    for i in range(0, len(file_names)):
        x = file_names[i].replace('.csv', '')
        z = x.replace('input_data/', '')
        filenames.append(z)
    # print(filenames)
    outdir_list = []
    for file in filenames:
        outdir ='output/'+ file+'/'
        outdir_list.append(outdir)
        if outdir[-1] != '/':
            outdir += '/'
        if not os.path.exists(outdir):
            os.makedirs(outdir)
          # A. Save TMax and TMax_sig for time series
    for df, fname, outdir in zip(df_to_save, filenames, outdir_list):
        df.to_csv(outdir + fname+ '_' + name + ".csv")

    return outdir_list

def convert_dates(df_HOMs_ls):

    for HOM_df in (df_HOMs_ls):  # change time stamp to python date for ech df
        HOM_df.index = pd.to_datetime(HOM_df.index)
    return df_HOMs_ls

def remove_A_X_NO3_(df_HOMs_ls):
    # remove the annoying A_X_NO3_

    for df in df_HOMs_ls:

        name_ls = df.axes[1]
        name_ls_to_df = []

        for name in name_ls:
            # string is list of characters
            ion = list(name)
            del ion[-1]
            del ion[0:8]
            ion = ''.join(map(str, ion))
            name_ls_to_df.append(ion)
        df.columns = name_ls_to_df

    return df_HOMs_ls

def make_bar_chart (homs_df_ls, topx, outdir_ls, monomer = True, peak = True):

        for homs_df, outdir in zip(homs_df_ls, outdir_ls):
            outdir = outdir
            if outdir[-1] != '/':
                outdir += '/'
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            compound = homs_df.index.values
            label = ''
            plot_name = ''
            #plot only the 10 highest
            x = compound[0:topx]
            y = []

            if monomer:
                if peak:
                    y = homs_df['peak_contr'].values
                    label = '% contribution to total monomer peak signal'
                    plot_name = 'monomer_peak_contr'
                else:
                    y = homs_df['steady_contr'].values
                    label = '% contribution to total monomer steady state signal'
                    plot_name = 'monomer_steady_contr'
            else:
                if peak:
                    y = homs_df['peak_contr'].values
                    label = '% contribution to total dimer peak signal'
                    plot_name = 'dimer_peak_contr'
                else:
                    y = homs_df['steady_contr'].values
                    label = '% contribution to total dimer steady state signal'
                    plot_name = 'dimer_steady_contr'

            y_pos = np.arange(len(x))
            contr = y[0:topx]

            plt.bar(y_pos, contr, align='center', alpha=0.5)
            plt.xticks(y_pos, x, rotation=45)
            plt.ylabel(label)
            plt.title(outdir)
            # plt.show()
            plt.tight_layout()
            plt.savefig(outdir + plot_name+' top'+ str(topx) + '.png', dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format=None,
                        transparent=False, bbox_inches=None, pad_inches=0.1,
                        frameon=None)
            plt.close()

def get_axis_limits(ax, scale=.8):
    return ax.get_xlim()[1]*scale, ax.get_ylim()[1]*scale

def make_overview_plot (HOMs_monomer_pcontr_df_ls, HOMs_monomer_scontr_df_ls, HOMs_dimer_pcontr_df_ls,
                        HOMs_dimer_scontr_df_ls, HOMs_mono_signal_df_ls, HOMs_di_signal_df_ls, top_compounds, outdir_ls):


    #save one overview plot per ppb experiment, means per outdir in outdir_ls!

    for i in range (0, len(outdir_ls)):

        outdir = outdir_ls[i]
        title = list(outdir)
        del title[-1]
        del title[0:7]
        try:
            a = int(title[-2])
            xppb = ''.join(map(str, title[-2::]))
        except:
                xppb = title[-1]

        fig_title = xppb+' ppb TMB'


        #create the figure
        fig = plt.figure(figsize=(9,6))
        fig.suptitle(fig_title, y=1)


        # make mono peak subplot
        mono_peak_compound = HOMs_monomer_pcontr_df_ls[i].index.values
        chemlist = []
        for k in range (0, len(mono_peak_compound[0:top_compounds])+1):
            if k ==0:
                chemlist.append(mono_peak_compound[0])
            else:
                chemlist.append(mono_peak_compound[k-1])

        chem_list = [re.sub("([0-9]+)", "_{\\1}", k) for k in chemlist]
        chem_list = ['$\mathregular{' + list_k + '}$' for list_k in chem_list]
        y_pos = np.arange(len(mono_peak_compound[0:top_compounds]))
        y_mp = HOMs_monomer_pcontr_df_ls[i]['peak_contr'].values
        contr = y_mp[0:top_compounds]
        ax1 = fig.add_subplot(321)# top left
        ax1.set_title('monomer initial peak' , size = 8)
        ax1.bar(y_pos, contr, align='center', alpha=0.5)
        # ax1.xaxis.set_major_locator(plticker.FixedLocator((y_pos)-1))
        # ax1.xaxis.set_major_formatter(plticker.FixedFormatter((x_mp)))
        # plt.xticks(rotation=30)
        ax1.set_xticklabels(chem_list)
        ax1.set_ylabel('% contribution')
        sum_mpcontr = str(HOMs_monomer_pcontr_df_ls[i].loc[chemlist[-1], 'peak_contr_sum'])
        sum_mpcontr = chr(931) + sum_mpcontr[0:5] +'%'
        ax1.annotate(sum_mpcontr, xy=get_axis_limits(ax1))

        # make mono steady subplot
        mono_steady_compound = HOMs_monomer_scontr_df_ls[i].index.values
        chemlist = []
        for k in range (0, len(mono_steady_compound[0:top_compounds])+1):
            if k ==0:
                chemlist.append(mono_steady_compound[0])
            else:
                chemlist.append(mono_steady_compound[k-1])
        chem_list = [re.sub("([0-9]+)", "_{\\1}", k) for k in chemlist]
        chem_list = ['$\mathregular{' + list_k + '}$' for list_k in chem_list]
        y_pos = np.arange(len(mono_steady_compound[0:top_compounds]))
        y_ms = HOMs_monomer_scontr_df_ls[i]['steady_contr'].values
        contr = y_ms[0:top_compounds]
        ax2 = fig.add_subplot(322, sharey = ax1)
        ax2.set_title("monomer steady state", size = 8)
        plt.setp(ax2.get_yticklabels(), visible=False)
        ax2.bar(y_pos, contr, align='center', alpha=0.5)
        ax2.set_xticklabels(chem_list)
        sum_mscontr = str(HOMs_monomer_scontr_df_ls[i].loc[chemlist[-1], 'steady_contr_sum'])
        sum_mscontr = chr(931) + sum_mscontr[0:5] +'%'
        ax2.annotate(sum_mscontr, xy=get_axis_limits(ax2))


        # make dimer peak subplot
        di_peak_compound = HOMs_dimer_pcontr_df_ls[i].index.values
        chemlist = []
        for k in range (0, len(di_peak_compound[0:top_compounds])+1):
            if k ==0:
                chemlist.append(di_peak_compound[0])
            else:
                chemlist.append(di_peak_compound[k-1])
        chem_list = [re.sub("([0-9]+)", "_{\\1}", k) for k in chemlist]
        chem_list = ['$\mathregular{' + list_k + '}$' for list_k in chem_list]
        y_pos = np.arange(len(di_peak_compound[0:top_compounds]))
        y_dp = HOMs_dimer_pcontr_df_ls[i]['peak_contr'].values
        contr = y_dp[0:top_compounds]
        ax3 = fig.add_subplot(323)# bottom left
        ax3.set_title("dimer initial peak", size = 8)
        ax3.bar(y_pos, contr, align='center', alpha=0.5)
        ax3.set_xticklabels(chem_list)
        ax3.set_ylabel('% contribution')
        sum_dpcontr = str(HOMs_dimer_pcontr_df_ls[i].loc[chemlist[-1], 'peak_contr_sum'])
        sum_dpcontr = chr(931) + sum_dpcontr[0:5] +'%'
        ax3.annotate(sum_dpcontr, xy=get_axis_limits(ax3))

        # make dimer steady subplot
        di_steady_compound = HOMs_dimer_scontr_df_ls[i].index.values
        chemlist = []
        for k in range (0, len(di_steady_compound[0:top_compounds])+1):
            if k ==0:
                chemlist.append(di_steady_compound[0])
            else:
                chemlist.append(di_steady_compound[k-1])
        chem_list = [re.sub("([0-9]+)", "_{\\1}", k) for k in chemlist]
        chem_list = ['$\mathregular{' + list_k + '}$' for list_k in chem_list]
        y_pos = np.arange(len(di_steady_compound[0:top_compounds]))
        y_ds = HOMs_dimer_scontr_df_ls[i]['steady_contr'].values
        contr = y_ds[0:top_compounds]
        ax4 = fig.add_subplot(324, sharey = ax3)# bottom right
        ax4.set_title("dimer steady state", size = 8)
        plt.setp(ax4.get_yticklabels(), visible=False)
        ax4.bar(y_pos, contr, align='center', alpha=0.5)
        ax4.set_xticklabels(chem_list)
        sum_dscontr = str(HOMs_dimer_scontr_df_ls[i].loc[chemlist[-1], 'steady_contr_sum'])
        sum_dscontr = chr(931) + sum_dscontr[0:5] +'%'
        ax4.annotate(sum_dscontr, xy=get_axis_limits(ax4))


        for ax in fig.axes:
            loc = plticker.MultipleLocator(base=1.0)  # this locator puts ticks at regular intervals
            ax.xaxis.set_major_locator(loc)
            plt.sca(ax)
            plt.xticks(rotation=70, fontsize = 12)

        #make signal overview plot
        ax5 = fig.add_subplot(313)
        time = HOMs_mono_signal_df_ls[i].index.values
        mono_signal = HOMs_mono_signal_df_ls[i]['monomer_sum'].values
        di_signal = HOMs_di_signal_df_ls[i]['dimer_sum'].values
        ax5.plot(time, mono_signal, color = 'blue', label = 'monomers')
        ax5.plot(time, di_signal, color = 'red', label = 'dimers')
        xfmt = md.DateFormatter('%H:%M')
        ax5..set_major_formatter(xfmt)
        ax5.set_ylabel('total signal')
        ax5.legend(loc = 1, fontsize = 8)
        plt.tight_layout()

        plt.savefig(outdir + fig_title + '.png', dpi=None, facecolor='w', edgecolor='w')
        plt.close()





def make_HOMs_df_ls(df_HOMs_ls, peak_end):
    HOMs_contribution_df_ls = []
    HOMs_monomer_contr_df_ls = []
    HOMs_dimer_contr_df_ls = []
    HOMs_monomer_pcontr_df_ls = []
    HOMs_monomer_scontr_df_ls = []
    HOMs_dimer_pcontr_df_ls = []
    HOMs_dimer_scontr_df_ls = []
    HOMs_mono_signal_df_ls = []
    HOMs_di_signal_df_ls = []

    for HOM_df in (df_HOMs_ls):  # do peak fitting for every df in the df list

        HOMs_contribution_df, HOMs_monomer_contr_df, HOMs_dimer_contr_df, \
        HOMs_monomer_pcontr_df, HOMs_monomer_scontr_df, HOMs_dimer_pcontr_df, HOMs_dimer_scontr_df = calculate_contributions(HOM_df, peak_end)  # call peakfitting function for every dataframe (
        # has internal loop in itself!)
        HOMs_contribution_df_ls.append(HOMs_contribution_df)
        HOMs_monomer_contr_df_ls.append(HOMs_monomer_contr_df)
        HOMs_dimer_contr_df_ls.append(HOMs_dimer_contr_df)
        HOMs_monomer_pcontr_df_ls.append(HOMs_monomer_pcontr_df)
        HOMs_monomer_scontr_df_ls.append(HOMs_monomer_scontr_df)
        HOMs_dimer_pcontr_df_ls.append(HOMs_dimer_pcontr_df)
        HOMs_dimer_scontr_df_ls.append(HOMs_dimer_scontr_df)

        HOMs_mono_signal, HOMs_di_signal = make_signal_sum(HOM_df)
        HOMs_mono_signal_df_ls.append(HOMs_mono_signal)
        HOMs_di_signal_df_ls.append(HOMs_di_signal)

    return HOMs_contribution_df_ls, HOMs_monomer_contr_df_ls, HOMs_dimer_contr_df_ls,\
           HOMs_monomer_pcontr_df_ls, HOMs_monomer_scontr_df_ls, HOMs_dimer_pcontr_df_ls,\
           HOMs_dimer_scontr_df_ls, HOMs_mono_signal_df_ls, HOMs_di_signal_df_ls

def make_signal_sum(HOM_df):

    HOM_monomer_signal_sum = pd.DataFrame(data={},
                       index=HOM_df.index)
    HOM_monomer_signal_sum.index.name = "time"

    HOM_dimer_signal_sum = pd.DataFrame(data={},
                       index=HOM_df.index)
    HOM_dimer_signal_sum.index.name = "time"

    # df['e'] = df.sum(axis=1)

    name_ls = HOM_df.axes[1]
    monomers = []
    dimers = []

    for name in name_ls:
        # string is list of characters
        ion = list(name)
        if ion[1] == '9':
            ion = ''.join(map(str, ion))
            monomers.append(ion)
        else:
            ion = ''.join(map(str, ion))
            dimers.append(ion)

    for i in range (0, len(name_ls)):
        ion = HOM_df.columns[i]
        if ion in monomers:
            # HOMs_contribution_df.loc[monomer, 'peak_signal']
            HOM_monomer_signal_sum[ion] = HOM_df[ion]
        else:
            HOM_dimer_signal_sum[ion] = HOM_df[ion]


    #sums up over index
    HOM_monomer_signal_sum['monomer_sum'] = HOM_monomer_signal_sum.sum(axis=1)
    HOM_dimer_signal_sum['dimer_sum'] = HOM_dimer_signal_sum.sum(axis=1)

    return HOM_monomer_signal_sum, HOM_dimer_signal_sum

def calculate_contributions(HOM_df, peak_end):  # calculate contribution for every ion in df


    # create emtpy lists
    peak_signal_ls = []
    steady_state_signal_ls = []
    contribution_total_ls = []



    # caclulate peak and steady state signal for every ion in df in list
    for i in range(0, len(HOM_df.columns)):
        ion_name = HOM_df.columns[i]
        time = HOM_df.index.values
        signal = HOM_df[ion_name].values

        peak_signal = 0 #calculate the sum
        for j in range (0,peak_end):
            if j == 0:
                peak_signal = signal[j]
            else:
               peak_signal = peak_signal+signal[j]

        steady_state_signal = 0
        for k in range(peak_end, len(signal)): #calculate the average
            if k == peak_end:
                steady_state_signal = signal[k]
            else:
                steady_state_signal = steady_state_signal + signal[k]
        steady_state_signal = steady_state_signal/ (len(signal)-peak_end)

        peak_signal_ls.append(peak_signal)
        steady_state_signal_ls.append(steady_state_signal)

    #calculate the contribution of each compound to the total
    contribution_total_ls = []
    for l in range(0, len(peak_signal_ls)):
        contribution_total = (peak_signal_ls[l] / sum(peak_signal_ls) * 100)
        contribution_total_ls.append(contribution_total)

    # create data frame from list of peak and steady state
    HOMs_contribution_df = pd.DataFrame(data={'peak_signal': peak_signal_ls,
                             'steady_state_signal': steady_state_signal_ls,
                             'contribution_to_total': contribution_total_ls},
                       index=HOM_df.columns.values)
    HOMs_contribution_df.index.name = "Molecule"




    #calculate monomer and dimer specific contributions
    name_ls = HOM_df.axes[1]
    monomers = []
    dimers = []

    for name in name_ls:
        # string is list of characters
        ion = list(name)
        if ion[1] == '9':
            ion = ''.join(map(str, ion))
            monomers.append(ion)
        else:
            ion = ''.join(map(str, ion))
            dimers.append(ion)

    monomer_peak_ls = []
    monomer_steady_ls = []
    dimer_peak_ls = []
    dimer_steady_ls = []
    for monomer in monomers:
        monomer_peak = HOMs_contribution_df.loc[monomer, 'peak_signal']
        monomer_peak_ls.append(monomer_peak)
        monomer_steady =HOMs_contribution_df.loc[monomer, 'steady_state_signal']
        monomer_steady_ls.append(monomer_steady)
    for dimer in dimers:
        dimer_peak = HOMs_contribution_df.loc[dimer, 'peak_signal']
        dimer_peak_ls.append(dimer_peak)
        dimer_steady = HOMs_contribution_df.loc[dimer, 'steady_state_signal']
        dimer_steady_ls.append(dimer_steady)

    monomer_peak_contr_ls = []
    monomer_steady_contr_ls = []
    dimer_peak_contr_ls = []
    dimer_steady_contr_ls = []
    for i in range (0,len(monomers)):
        monomer_pcontr = (monomer_peak_ls[i]/ sum(monomer_peak_ls))*100
        monomer_peak_contr_ls.append(monomer_pcontr)
        monomer_scontr = (monomer_steady_ls[i]/ sum(monomer_steady_ls))*100
        monomer_steady_contr_ls.append(monomer_scontr)
    for i in range (0,len(dimers)):
        dimer_pcontr = (dimer_peak_ls[i]/ sum(dimer_peak_ls))*100
        dimer_peak_contr_ls.append(dimer_pcontr)
        dimer_scontr = (dimer_steady_ls[i]/ sum(dimer_steady_ls))*100
        dimer_steady_contr_ls.append(dimer_scontr)



    HOMs_monomer_contr_df = pd.DataFrame(data={'monomer_peak': monomer_peak_ls, 'monomer_steady': monomer_steady_ls,
                             'peak_contr' : monomer_peak_contr_ls, 'steady_contr': monomer_steady_contr_ls},
                              index=monomers)
    HOMs_monomer_contr_df.index.name = "Monomer"


    HOMs_dimer_contr_df = pd.DataFrame(data={'dimer_peak': dimer_peak_ls, 'dimer_steady': dimer_steady_ls,
                             'peak_contr' : dimer_peak_contr_ls, 'steady_contr': dimer_steady_contr_ls },
                              index=dimers)
    HOMs_dimer_contr_df.index.name = "Dimer"

    #make new df with decending order
    HOMs_monomer_pcontr_df = HOMs_monomer_contr_df.sort_values(by='peak_contr', ascending=0)
    HOMs_monomer_scontr_df = HOMs_monomer_contr_df.sort_values(by='steady_contr', ascending=0)
    HOMs_dimer_pcontr_df = HOMs_dimer_contr_df.sort_values(by='peak_contr', ascending=0)
    HOMs_dimer_scontr_df = HOMs_dimer_contr_df.sort_values(by='steady_contr', ascending=0)

    # calculate top 10 contributors
    sorted_peak_monomers = HOMs_monomer_pcontr_df.index.values
    sorted_steady_monomers = HOMs_monomer_scontr_df.index.values
    sorted_peak_dimers = HOMs_dimer_pcontr_df.index.values
    sorted_steady_dimers = HOMs_dimer_scontr_df.index.values

    monomer_pcontr_sum = []
    monomer_scontr_sum = []
    dimer_pcontr_sum = []
    dimer_scontr_sum = []

    for i in range(0, len(monomers)):
        if i == 0:
            pmonomer = sorted_peak_monomers[i]
            smonomer = sorted_steady_monomers[i]
            monomer_pcontr = HOMs_monomer_contr_df.loc[pmonomer, 'peak_contr']
            monomer_pcontr_sum.append(monomer_pcontr)
            monomer_scontr = HOMs_monomer_contr_df.loc[smonomer, 'steady_contr']
            monomer_scontr_sum.append(monomer_scontr)
        else:
            pmonomer = sorted_peak_monomers[i]
            smonomer = sorted_steady_monomers[i]
            monomer_pcontr = HOMs_monomer_contr_df.loc[pmonomer, 'peak_contr'] + monomer_pcontr_sum[i - 1]
            monomer_pcontr_sum.append(monomer_pcontr)
            monomer_scontr = HOMs_monomer_contr_df.loc[smonomer, 'steady_contr'] + monomer_scontr_sum[i - 1]
            monomer_scontr_sum.append(monomer_scontr)

    for i in range(0, len(dimers)):
        if i == 0:
            pdimer = sorted_peak_dimers[i]
            sdimer = sorted_steady_dimers[i]
            dimer_pcontr = HOMs_dimer_contr_df.loc[pdimer, 'peak_contr']
            dimer_pcontr_sum.append(dimer_pcontr)
            dimer_scontr = HOMs_dimer_contr_df.loc[sdimer, 'steady_contr']
            dimer_scontr_sum.append(dimer_scontr)
        else:
            pdimer = sorted_peak_dimers[i]
            sdimer = sorted_steady_dimers[i]
            dimer_pcontr = HOMs_dimer_contr_df.loc[pdimer, 'peak_contr'] + dimer_pcontr_sum[i - 1]
            dimer_pcontr_sum.append(dimer_pcontr)
            dimer_scontr = HOMs_dimer_contr_df.loc[sdimer, 'steady_contr'] + dimer_scontr_sum[i - 1]
            dimer_scontr_sum.append(dimer_scontr)

    # add su contribution to df
    HOMs_monomer_pcontr_df['peak_contr_sum'] = monomer_pcontr_sum
    HOMs_monomer_scontr_df['steady_contr_sum'] = monomer_scontr_sum
    HOMs_dimer_pcontr_df['peak_contr_sum'] = dimer_pcontr_sum
    HOMs_dimer_scontr_df['steady_contr_sum'] = dimer_scontr_sum


    return HOMs_contribution_df, HOMs_monomer_contr_df, HOMs_dimer_contr_df,\
           HOMs_monomer_pcontr_df, HOMs_monomer_scontr_df, HOMs_dimer_pcontr_df, HOMs_dimer_scontr_df








def example1():
    # ------------------------------- File I/O and Data Cleaning Example -------------------------------- #


    indir = "topython/"  # input directory
    infiles =  [f for f in os.listdir(indir) if f.endswith('.csv')]  # input all files in folder as a list of strings
    #
    # infiles = ['ppb17.csv']  # input test file as a list of strings


    # Read in list of csvs with TMB HOMs peak and steady state signal, index is time!
    df_HOMs_ls = pio.read_files(fdir=indir, flist=infiles)
    pio.set_idx_ls(df_HOMs_ls, idx_name='t_start')

    df_HOMs_ls = convert_dates(df_HOMs_ls)
    df_HOMs_ls = remove_A_X_NO3_(df_HOMs_ls)

    #this is the datapoint where the peak ends and steady state begins, Must be checked manually
    peak_end = 20
    top_compounds = 10

    # Clean ion names from default A_CxHyOzI_AvP format (strip underscores '_' and remove avP
   #  for df in df_HOMs_ls:
   #      #print("Example of ion names before clean: ", df.columns.values[0:3])
   #      df.columns = gen_chem.cln_molec_names(idx_names=df.columns.values, delim="_")  # remove underscores and avP
   # #      #df.columns = gen_chem.replace_group(molec_ls=df.columns.values, old_groups=["I"], new_group="")
   #      #print('Example of ion names after clean: ', df.columns.values[0:3])


    # print(df_HOMs_ls)
    HOMs_contribution_df_ls, HOMs_monomer_contr_df_ls, HOMs_dimer_contr_df_ls, \
    HOMs_monomer_pcontr_df_ls, HOMs_monomer_scontr_df_ls, HOMs_dimer_pcontr_df_ls, \
    HOMs_dimer_scontr_df_ls, HOMs_mono_signal_df_ls, HOMs_di_signal_df_ls = make_HOMs_df_ls(df_HOMs_ls, peak_end)



    outdir_ls = save_results(indir, HOMs_contribution_df_ls, 'HOMS_contr')
    save_results(indir, HOMs_monomer_contr_df_ls, 'Monomer_contr')
    save_results(indir, HOMs_dimer_contr_df_ls, 'Dimer_contr')
    save_results(indir, HOMs_monomer_pcontr_df_ls, 'Monomer_Pcontr')
    save_results(indir, HOMs_monomer_scontr_df_ls, 'Dimer_Scontr')
    save_results(indir, HOMs_dimer_pcontr_df_ls, 'Monomer_Pcontr')
    save_results(indir, HOMs_dimer_scontr_df_ls, 'Dimer_Scontr')

    # make_bar_chart(HOMs_monomer_pcontr_df_ls, top_compounds, outdir_ls, monomer=True, peak=True)
    # make_bar_chart(HOMs_monomer_scontr_df_ls, top_compounds, outdir_ls, monomer=True, peak=False)
    # make_bar_chart(HOMs_dimer_pcontr_df_ls, top_compounds, outdir_ls, monomer=False, peak=True)
    # make_bar_chart(HOMs_dimer_scontr_df_ls, top_compounds, outdir_ls, monomer=False, peak=False)

    make_overview_plot (HOMs_monomer_pcontr_df_ls, HOMs_monomer_scontr_df_ls, HOMs_dimer_pcontr_df_ls,
                        HOMs_dimer_scontr_df_ls, HOMs_mono_signal_df_ls, HOMs_di_signal_df_ls, top_compounds, outdir_ls)




    return 0

if __name__ == '__main__':
    example1()
