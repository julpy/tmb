
import os
import matplotlib
matplotlib.use('Qt5Agg')   # generate postscript output by default

import numpy as np
from scipy import optimize
from pygaero import pio
from pygaero import therm
from pygaero import gen_chem
import datetime as dt


from scipy.special import erf
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
from pygaero import therm
import numpy as np
from scipy.signal import argrelmin
from scipy.signal import argrelmax

import matplotlib.ticker as plticker
import matplotlib.dates as md
import re


def convert_dates(df_ls):
    for df in (df_ls):  # change time stamp to python date for ech df
            df.index = pd.to_datetime(df.index)
    return df_ls
def convert_dates_single(df):
    df.index = pd.to_datetime(df.index)
    return df

def get_axis_limits(ax, scale=1.2):
    return ax.get_xlim()[1]*scale, ax.get_ylim()[1]*scale




def make_plot_NOx(df_ls,comments_df_ls ):
    O3_df_ls = []
    NOx_df_ls = []

    for i in range (0, len(df_ls)):
        compound = df_ls[i].columns.tolist()
        if compound[0] == 'O3':
            O3_df_ls.append(df_ls[i])
        else:
            NOx_df_ls.append(df_ls[i])


    for O3_df, NOx_df, comment_df in zip(O3_df_ls, NOx_df_ls, comments_df_ls):

        NOx_time = NOx_df.index.values
        O3_time = O3_df.index.values

        NO = NOx_df['NO'].values
        NO2 = NOx_df['NO2'].values
        NOx = NOx_df['NOx'].values
        O3 = O3_df['O3'].values


        NO_label = 'NO'
        NO2_label = '$\mathregular{NO_2}$'
        NOx_label = 'NOx'
        O3_label = '$\mathregular{O_3}$'


        comments_ls=[]
        # make comment dataframes and add lines to mark where conditions change, also use time in comments
        # automatillay plot the different ratios!
        high_df = pd.DataFrame(data={'comment': comment_df.index},index=comment_df['high'].values)
        high_df.index.name = "time"
        high_df = convert_dates_single(high_df)
        comments_ls.append(high_df)
        med_df = pd.DataFrame(data={'comment': comment_df.index},index=comment_df['medium'].values)
        med_df.index.name = "time"
        med_df = convert_dates_single(med_df)
        comments_ls.append(med_df)
        low_df = pd.DataFrame(data={'comment': comment_df.index},index=comment_df['low'].values)
        low_df.index.name = "time"
        low_df = convert_dates_single(low_df)
        comments_ls.append(low_df)

        fig1 = plt.figure()

        #make 3 subplots, one for each O3/NOx
        ratio_h = 'high'
        ratio_m = 'medium'
        ratio_l = 'low'
        ax1 = fig1.add_subplot(311)
        ax2 = fig1.add_subplot(312)
        ax3 = fig1.add_subplot(313)
        ax1.set_title(ratio_h)
        ax2.set_title(ratio_m)
        ax3.set_title(ratio_l)
        ax1.plot(O3_time, O3, label=O3_label, color='blue')
        ax1.plot(NOx_time, NO, label=NO_label, color='red')
        ax1.plot(NOx_time, NO2, label=NO2_label, color='green')
        ax1.plot(NOx_time, NOx, label=NOx_label, color='black')
        ax1.set_ylabel('ppb')
        ax1.legend(loc='upper right')

        ax2.plot(O3_time, O3, label=O3_label, color='blue')
        ax2.plot(NOx_time, NO, label=NO_label, color='red')
        ax2.plot(NOx_time, NO2, label=NO2_label, color='green')
        ax2.plot(NOx_time, NOx, label=NOx_label, color='black')
        ax2.set_ylabel('ppb')

        ax3.plot(O3_time, O3, label=O3_label, color='blue')
        ax3.plot(NOx_time, NO, label=NO_label, color='red')
        ax3.plot(NOx_time, NO2, label=NO2_label, color='green')
        ax3.plot(NOx_time, NOx, label=NOx_label, color='black')
        ax3.set_ylabel('ppb')

        xfmt = md.DateFormatter('%H:%M')
        ax1.xaxis.set_major_formatter(xfmt)
        ax2.xaxis.set_major_formatter(xfmt)
        ax3.xaxis.set_major_formatter(xfmt)

        #zoom in to proper time intervals

        max = get_axis_limits(ax1)
        df_H = comments_ls[0]
        comments_time_h = df_H.index.values
        comments_h = df_H['comment'].values
        y_position = max[1]
        ax1.vlines(x=comments_time_h, ymin=0, ymax=max[1])
        for i, time in enumerate(comments_time_h):
            ax1.text(time, y_position, comments_h[i], rotation=90, verticalalignment='top')
        ax1.set_xlim([comments_time_h[0], comments_time_h[-1]])
        #  start = time_range[0]-dt.timedelta(seconds=300)
        # end = time_range[-1]+dt.timedelta(seconds=300)



        max = get_axis_limits(ax2)
        df_m = comments_ls[1]
        comments_time_m = df_m.index.values
        comments_m = df_m['comment'].values
        y_position = max[1]
        ax2.vlines(x=comments_time_m, ymin=0, ymax=max[1])
        for i, time in enumerate(comments_time_m):
            ax2.text(time, y_position, comments_m[i], rotation=90, verticalalignment='top')
        ax2.set_xlim([comments_time_m[0], comments_time_m[-1]])
        #  start = time_range[0]-dt.timedelta(seconds=300)
        # end = time_range[-1]+dt.timedelta(seconds=300)



        max = get_axis_limits(ax3)
        df_l = comments_ls[2]
        comments_time_l = df_l.index.values
        comments_l = df_l['comment'].values
        y_position = max[1]
        ax3.vlines(x=comments_time_l, ymin=0, ymax=max[1])
        for i, time in enumerate(comments_time_l):
            ax3.text(time, y_position, comments_l[i], rotation=90, verticalalignment='top')
        ax3.set_xlim([comments_time_l[0], comments_time_l[-1]])
        #  start = time_range[0]-dt.timedelta(seconds=300)
        # end = time_range[-1]+dt.timedelta(seconds=300)


        plt.tight_layout()









def example1():
    # ------------------------------- File I/O and Data Cleaning Example -------------------------------- #



    # load NOx and O3 data
    indir = "topython/O3NOxdata/"  # input directory
    infiles = [f for f in os.listdir(indir) if f.endswith('.csv')]  # input all files in folder as a list of strings
    # infiles = ['ppb17.csv']  # input test file as a list of strings
    # Read in list of csvs with TMB HOMs peak and steady state signal, index is time!
    NOxO3_df_ls = pio.read_files(fdir=indir, flist=infiles)
    pio.set_idx_ls(NOxO3_df_ls, idx_name='time')
    NOxO3_df_ls = convert_dates(NOxO3_df_ls)


    # load comments
    indir = "topython/O3NOxdata/comments/"  # input directory
    infiles = [f for f in os.listdir(indir) if f.endswith('.csv')]  # input all files in folder as a list of strings
    # infiles = ['ppb17.csv']  # input test file as a list of strings
    # Read in list of csvs with TMB HOMs peak and steady state signal, index is time!
    comments_df_ls = pio.read_files(fdir=indir, flist=infiles)
    pio.set_idx_ls(comments_df_ls, idx_name='comment')



    make_plot_NOx(NOxO3_df_ls, comments_df_ls)

    return 0

if __name__ == '__main__':
    example1()
